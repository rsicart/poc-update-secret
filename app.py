import logging
import authenticator
import update_secret
import os
import sys

if __name__ == '__main__':

    # setup logger
    log_level = logging.DEBUG
    FORMAT = '%(asctime)-15s %(message)s'
    logging.basicConfig(level=log_level, format=FORMAT)
    logger = logging.getLogger(__name__)

    # get env vars
    url = os.environ.get('APP_URL', None)
    if not url:
        logger.error("Environment variable APP_URL is empty.")
        sys.exit(1)

    username = os.environ.get('APP_USERNAME', None)
    if not username:
        logger.error("Environment variable APP_USERNAME is empty.")
        sys.exit(1)

    password = os.environ.get('APP_PASSWORD', None)
    if not password:
        logger.error("Environment variable APP_PASSWORD is empty.")
        sys.exit(1)

    in_cluster_config = os.environ.get('APP_IN_CLUSTER_CONFIG', False)
    if not in_cluster_config:
        logger.debug("Environment variable APP_IN_CLUSTER_CONFIG is empty.")
    else:
        logger.debug("Environment variable APP_IN_CLUSTER_CONFIG is True.")
        in_cluster_config = True

    secret_namespace = os.environ.get('APP_TARGET_SECRET_NAMESPACE', None)
    if not secret_namespace:
        logger.error("Environment variable APP_TARGET_SECRET_NAMESPACE is empty.")
        sys.exit(1)

    secret_name = os.environ.get('APP_TARGET_SECRET_NAME', None)
    if not secret_name:
        logger.error("Environment variable APP_TARGET_SECRET_NAME is empty.")
        sys.exit(1)

    # fetch token
    auth = authenticator.Authenticator(logger=logger, url=url, username=username, password=password)
    token = auth.authenticate()
    if not token:
        logger.error("Something went wrong fetching the token.")
        sys.exit(1)

    logger.debug("Token fetched!")

    # update secret
    su = update_secret.SecretUpdater(logger=logger, in_cluster_config=in_cluster_config)
    exists = su.exists(namespace=secret_namespace, name=secret_name)

    labels = {'app': 'secret-updater'}
    data = {'token': token}

    if not exists:
        su.create_secret(namespace=secret_namespace, name=secret_name, labels=labels, data=data)
    else:
        su.update_secret(namespace=secret_namespace, name=secret_name, labels=labels, data=data)
