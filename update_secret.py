from kubernetes import client, config
import logging


class SecretUpdater:

    def __init__(self, logger=None, in_cluster_config=False):
        if in_cluster_config:
            config.load_incluster_config()
        else:
            config.load_kube_config()
        self.v1 = client.CoreV1Api()
        self.logger = logger or logging.getLogger(__name__)

    def exists(self, namespace=None, name=None):
        """
        Check if a Secret exists
        :param str namespace: object name and auth scope, such as for teams and projects (required)
        :param str name: name of the Secret (required)
        :return: boolean
        """
        try:
            self.logger.debug("Does secret {} exist in namespace {} ?".format(name, namespace))
            ret = self.v1.read_namespaced_secret(namespace=namespace, name=name)
            self.logger.error("Secret already exists")
            return True
        except client.rest.ApiException as e:
            if e.status == 404:
                self.logger.error("Secret does not exist")
                return False
            else:
                raise e

    def create_secret(self, namespace=None, name=None, labels=None, data=None):
        """
        Create a Secret
        :param str namespace: object name and auth scope, such as for teams and projects (required)
        :param str name: name of the Secret (required)
        :param dict(str, str) labels
        :param dict(str, str) data
        :return: boolean
        """

        meta = client.V1ObjectMeta(labels=labels, name=name, namespace=namespace)
        body = client.V1Secret(string_data=data, metadata=meta) 
        self.logger.info("Creating Secret {} in namespace {}".format(name, namespace))
        self.v1.create_namespaced_secret(namespace, body)

    def get_secret(self, namespace=None, name=None):
        """
        Get a Secret
        :param str namespace: object name and auth scope, such as for teams and projects (required)
        :param str name: name of the Secret (required)
        :return: None or V1Secret
        """
        try:
            self.logger.info("Reading secret {} in namespace: {}".format(name, namespace))
            ret = self.v1.read_namespaced_secret(namespace=namespace, name=name)
            return ret
        except client.rest.ApiException as e:
            if e.status == 404:
                self.logger.error("Secret does not exist")
                return None
            else:
                raise e

    def update_secret(self, namespace=None, name=None, labels=None, data=None):
        """
        Update a Secret
        :param str namespace: object name and auth scope, such as for teams and projects (required)
        :param str name: name of the Secret (required)
        :param dict(str, str) labels
        :param dict(str, str) data
        :return: boolean
        """
        meta = client.V1ObjectMeta(labels=labels, name=name, namespace=namespace)
        self.logger.info("Updating Secret {} in namespace {}".format(name, namespace))
        body = client.V1Secret(string_data=data, metadata=meta) 
        self.v1.patch_namespaced_secret(name=name, namespace=namespace, body=body)
