import logging
from urllib.parse import urlparse, urlunparse
from urllib.error import HTTPError, URLError
from urllib import request
from socket import timeout
import json
import base64


class Authenticator:
    """
    Get a token from a json endpoint using Basic HTTP
    """

    def __init__(self, logger=None, url=None, username=None, password=None, headers=None, timeout=15):
        self.logger = logger or logging.getLogger(__name__)
        self.set_url(url)
        self.set_username(username)
        self.set_password(password)
        self.set_headers(headers)
        self.set_timeout(timeout)

    def get_url(self):
        """
        Get URL build from a tuple using urllib.parse.urlunparse
        """
        return urlunparse(self.url)

    def set_url(self, url):
        """
        Set URL as a tuple returned by urllib.parse.urlparse
        param url str
        """
        try:
            parsed_tuple = urlparse(url)
        except ValueError as e:
            self.logger.error("Error parsing url")

        self.url = parsed_tuple

    def get_username(self):
        return self.username

    def set_username(self, username):
        """
        Sets username
        param username str
        """
        if not username:
            self.logger.warning("Username is empty")
        self.username = username

    def get_password(self):
        return self.password

    def set_password(self, password):
        """
        Sets password
        param password str
        """
        if not password:
            self.logger.warning("Password is empty")
        self.password = password

    def get_headers(self):
        return self.headers

    def set_headers(self, headers):
        """
        Sets headers to be added
        param headers dict
        """
        self.headers = headers

    def get_timeout(self):
        return self.timeout

    def set_timeout(self, timeout):
        """
        Sets timeout seconds for http client
        param timeout int seconds
        """
        self.timeout = timeout

    def get_authorization_value(self):
        data = '{}:{}'.format(self.get_username(), self.get_password())
        data_bytes = base64.b64encode(data.encode())
        return 'Basic {}'.format(data_bytes.decode())

    def do_post(self, req):
        """
        Makes post request to self.url.
        param req urllib.request.Request
        """
        # append headers
        req.add_header('Content-Type', 'application/json')
        req.add_header('Authorization', self.get_authorization_value())
        if self.get_headers():
            for header_name, header_value in self.get_headers().items():
                req.add_header(header_name, header_value)

        # make http call
        try:
            self.logger.debug("POST request sent to {} with data length: {}".format(self.get_url(), len(req.data.decode('utf-8'))))
            res = request.urlopen(req, timeout=self.get_timeout())
            return res
        except (HTTPError, URLError) as e:
            self.logger.error("HTTP error: {}".format(e))
        except timeout as e:
            self.logger.error("Timeout Error: {}".format(e))
        except Exception as e:
            self.logger.error("Unknown error: {}".format(e))
        return None

    def authenticate(self):
        """
        Authenticates to get the token
        """
        # prepare payload
        try:
            payload = {
                'username': self.get_username(),
                'password': self.get_password(),
            }
            payload_string = json.dumps(payload)
            payload_bytes = payload_string.encode('utf-8')
        except ValueError as e:
            self.logger.error("Error parsing json payload")
        req = request.Request(self.get_url(), data=payload_bytes)
        res = self.do_post(req)

        # parse response
        if not res:
            self.logger.error("Something went wrong with http response")
            return None
        body_bytes = res.read()
        body = body_bytes.decode()
        body_dict = json.loads(body)

        if 'id_token' in body_dict:
            if body_dict['id_token']:
                return body_dict['id_token']

        self.logger.error("Token not found in response")
        return None
