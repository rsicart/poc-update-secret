# Secret updater

Cronjob used to fetch a token from an endpoint and write it to a Kubernetes Secret.

This chart creates:
* a Cronjob
* a ServiceAccount, used by Cronjob's Pods
* a Role, to configure permissions on api endpoints and objects
* a RoleBinding, to setup Role for ServiceAccount


BEFORE installing this chart, you MUST create a Secret containing HTTP basic authentication credentials.
Values are base64 encoded !

## 1. Configure basic auth credentials

Add the following content to file `secret-updater-basic-auth.yaml`:

```
apiVersion: v1
data:
  username: dG90bw==
  password: dG90bw==
kind: Secret
metadata:
  labels:
    app: secret-updater
  name: secret-updater-basic-auth
type: Opaque
```

## 2. Create the secret in YOUR_NAMESPACE:

```
kubectl -n YOUR_NAMESPACE apply -f secret-updater-basic-auth.yaml
```

## 3. Override default values

Do not forget to create a custom `values-custom.yaml` file to override default settings !
In order to do it, you can copy the existing `values.yaml` file.


## 4. Install helm chart

```
helm upgrade --install secret-updater ./helm/ --namespace YOUR_NAMESPACE -f values-custom.yaml
```
